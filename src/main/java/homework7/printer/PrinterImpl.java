package homework7.printer;

import homework7.entity.Cell;

public class PrinterImpl implements Printer {

    public void print(Cell[][] desk, int size) {
        System.out.println("desk viwe:");

        for (int y = 0; y < size + 1; y++) {
            for (int x = 0; x < size + 1; x++) {
                System.out.print(desk[x][y].getStatus());
            }
            System.out.println();
        }
    }

    public void printAllData(Cell[][] desk, int size) {
        String changeMap = "Canges map: ";
        System.out.println(String.format("desk viwe:     %s", changeMap));
        StringBuffer line = new StringBuffer();
        for (int y = 0; y < size + 1; y++) {
            for (int x = 0; x < size + 1; x++) {
                line.append(desk[x][y].getStatus());
            }
            line.append("|  |");
            for (int x = 0; x < size + 1; x++) {
                if (!desk[x][y].isChenged()) {
                    line.append(desk[x][y].getStatus());
                } else {
                    line.append("^");
                }
            }
            line.append("|\n");
        }

        System.out.println(line.toString());
    }

    private void printDeskInLine(Cell[][] desk, int size) {
        System.out.println("\n\ndesk status:\n");
        for (int y = 0; y < size + 1; y++) {
            for (int x = 0; x < size + 1; x++) {
                System.out.print(desk[x][y].getStatus());
            }
        }
        System.out.print("|\n");
    }

    public void print(String name, int count) {
        System.out.println(String.format("%s - %d cells.", name, count));
    }

}
