package homework7.printer;

import homework7.entity.Cell;

public interface Printer {

    void print(Cell[][] desk, int size);
    void print(String name, int size);
    void printAllData(Cell[][] desk, int size);
    

}
