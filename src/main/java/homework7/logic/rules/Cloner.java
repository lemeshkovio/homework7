package homework7.logic.rules;

import java.util.List;

import homework7.entity.Cell;

public class Cloner extends Parent {

    public Cloner(Cell[][] desk) {
        super(desk);
    }

    public void clone(Cell[][] cells, List<Cell> changed) {
        for (Cell cell : changed) {
            if (getAliveAround(cell) == 3) {
                cell.setAlive();
            }
        }
    }

}
