package homework7.logic.rules;

import homework7.entity.Cell;
import homework7.entity.CellStatus;

public class Parent {
    
    protected Cell[][] cells;
    public Parent(Cell[][] desk) {
        cells = desk;
    }


    public int getAliveAround(Cell cell) {
        int x = cell.getX();
        int y = cell.getY();
        int aliveAround = 0;
        for (int i = y - 1; i < y + 1; i++) {
            for (int j = x - 1; j < x + 1; j++) {
                if (i == x && j == x) {
                    continue;
                }
                if (cells[j][i].getStatus().equals(CellStatus.LIVE.getDesc())) {
                    aliveAround++;
                }
            }
        }
        return aliveAround;
    }

    public Cell[][] getCells() {
        return cells;
    }

}
