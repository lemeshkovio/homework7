package homework7.logic.rules;

import java.util.ArrayList;
import java.util.List;

import homework7.entity.Cell;
import homework7.entity.CellStatus;

public class Cheker {

    private List<Cell> changed;
    private List<Cell> alive;

    public void check(Cell[][] cells) {
        changed = new ArrayList<>();
        alive = new ArrayList<>();
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells.length; x++) {
                if (cells[x][y].isChenged()) {
                    changed.add(cells[x][y]);
                }
                if (cells[x][y].getStatus().equals(CellStatus.LIVE.getDesc())) {
                    alive.add(cells[x][y]);
                }
            }
        }
    }

    public List<Cell> getchanged() {
        return changed;
    }
    public List<Cell> getAlive() {
        return alive;
    }
}
