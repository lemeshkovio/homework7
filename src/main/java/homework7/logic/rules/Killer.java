package homework7.logic.rules;

import java.util.List;

import homework7.entity.Cell;

public class Killer extends Parent {

    public Killer(Cell[][] desk) {
        super(desk);
    }

    public void kill(Cell[][] cells, List<Cell> alive) {

        for (Cell cell : alive) {
            if (getAliveAround(cell) > 3 || getAliveAround(cell) < 2) {
                cell.setDead();
            }
        }
    }
}
