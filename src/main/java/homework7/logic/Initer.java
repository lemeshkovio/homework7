package homework7.logic;

import homework7.entity.Desk;
import homework7.printer.Printer;
import homework7.printer.PrinterImpl;

public class Initer {
    private Desk desk;
    public final Printer printer = new PrinterImpl();
    
    
    public Initer(int size) {
        this.desk = new Desk(size);
        desk.createField();
    }
    
    public Desk getDesk() {
        return desk;
    }
    
    public void randomBorns(int count) {
        desk.randomSeed(count);
    }
    

}
