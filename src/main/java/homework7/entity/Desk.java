package homework7.entity;

import java.util.Random;

public class Desk {
    private final int deskSize;
    private Cell[][] desk;
    private final Random rand = new Random();

    /**
     * Input desk size
     * 
     * @param deskSize
     *            - size of game field
     */
    public Desk(int deskSize) {
        this.deskSize = deskSize;
        desk = new Cell[deskSize + 2][deskSize + 2];
    }

    /**
     * Creating a game fild
     * 
     * @return list of dead cells with size
     */
    public void createField() {
        for (int y = 0; y < deskSize + 2; y++) {
            for (int x = 0; x < deskSize + 2; x++) {
                desk[x][y] = new Cell(x, y);
            }
        }
    }

    /**
     * 
     * @param count
     *            - count of cells to randomly borns
     */
    public void randomSeed(int count) {

        while (count > 0) {
            int y = rand.nextInt(deskSize-1)+1;
            int x = rand.nextInt(deskSize-1)+1;
            if (desk[x][y].getStatus().equals(CellStatus.DEAD.getDesc())) {
                desk[x][y].setAlive();
                setChanged(x, y);
                count--;
            }
        }
    }

    private void setChanged(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                desk[i][j].setCangedTrue();
            }
        }
    }

    public Cell[][] getAll() {
        return desk;
    }

    public Cell get(int x, int y) {
        return desk[x][y];
    }

    public void born(int x, int y) {
        this.desk[x][y].setAlive();
    }

    public void die(int x, int y) {
        this.desk[x][y].setDead();
    }

    public void setDesk(Cell[][] desk) {

    }
}
