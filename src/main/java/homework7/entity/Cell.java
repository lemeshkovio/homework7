package homework7.entity;

public class Cell {
    private CellStatus status = CellStatus.DEAD;
    private final int xCoord;
    private final int yCoord;
    private boolean isChenged = false;
    

    public Cell(int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    
    public boolean isChenged() {
        return isChenged;
    }


    public void setCangedTrue() {
        this.isChenged = true;
    }
    
    public void setChangedFalse() {
        this.isChenged = false;
    }


    public int getX() {
        return xCoord;
    }

    public int getY() {
        return yCoord;
    }

    public String getStatus() {
        return status.getDesc();
    }

    public void setDead() {
        this.status = CellStatus.DEAD;
    }
    
    public void setAlive() {
        this.status = CellStatus.LIVE;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cell other = (Cell) obj;
        if (status != other.status)
            return false;
        return true;
    }


    @Override
    public String toString() {
        return "Cell (status = " + status + ", x=" + xCoord + ", y=" + yCoord + ", isChenged = " + isChenged
                + ")";
    }


    
    
    
}
