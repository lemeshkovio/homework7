package homework7.entity;

public enum CellStatus {DEAD(" "),LIVE("*");

    
    private final String desc;

    private CellStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
