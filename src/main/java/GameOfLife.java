
import homework7.logic.Initer;
import homework7.logic.rules.Cheker;
import homework7.logic.rules.Cloner;
import homework7.logic.rules.Killer;

public class GameOfLife {

    private static final int SIZE = 10;
    private static final int RAND_SEED_COUNT = 2;
    private static final String ALIVE = "Alive";
    private static final String CHANGED = "Changed";

    public static void main(String[] args) {

        Initer init = new Initer(SIZE);
        Cheker cheker = new Cheker();

        init.randomBorns(RAND_SEED_COUNT);
        var INIT = init.getDesk().getAll();
        cheker.check(INIT);
        Killer killer = new Killer(INIT);
        Cloner cloner = new Cloner(INIT);
        init.printer.printAllData(init.getDesk().getAll(), SIZE);
        init.printer.print(ALIVE, cheker.getAlive().size());
        init.printer.print(CHANGED, cheker.getchanged().size());
        int i =3;
        while (i > 1) {

            try {
                cloner.clone(INIT, cheker.getchanged());
            } catch (Exception e) {
                System.err.println("Can't clone\n" + e);
            }

            try {
                killer.kill(INIT, cheker.getchanged());
            } catch (Exception e) {
                System.err.println("Can't kill\n" + e);
            }
            init.printer.printAllData(init.getDesk().getAll(), SIZE);
            init.printer.print(ALIVE, cheker.getAlive().size());
            init.printer.print(CHANGED, cheker.getchanged().size());
            cheker.check(INIT);
            i--;
            clrscr();
            
        }

        // System.out.println("All cells are dead. Life is pain.");

    }

    private static void clrscr() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            System.err.println(e);
        }
        System.out.print("\033[H\033[2J");
    }

}
